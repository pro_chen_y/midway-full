import { Configuration, App, Inject, ILifeCycle } from '@midwayjs/core';
import * as koa from '@midwayjs/koa';
import * as validate from '@midwayjs/validate';
import * as info from '@midwayjs/info';
import { ReportMiddleware } from './middleware/report.middleware.js';
import DefaultConfig from './config/config.default.js';
import UnittestConfig from './config/config.unittest.js';

import { ServerSideRenderService } from './service/serverSideRender.service.js';

@Configuration({
  imports: [
    koa,
    validate,
    {
      component: info,
      enabledEnvironment: ['local'],
    },
  ],
  importConfigs: [
    {
      default: DefaultConfig,
      unittest: UnittestConfig,
    },
  ],
})
export class MainConfiguration implements ILifeCycle {
  @App('koa')
  app: koa.Application;

  @Inject()
  serverSideRenderService: ServerSideRenderService;

  async onReady() {
    // add middleware
    this.app.useMiddleware([ReportMiddleware]);

    await this.serverSideRenderService.startup();
  }

  async onStop() {
    await this.serverSideRenderService.stop();
  }
}
