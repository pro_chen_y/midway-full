import '@midwayjs/core';
import { InlineConfig } from 'vite';
import * as serve from 'koa-static';
import { Context } from 'koa';

/**
 * @description User-Service parameters
 */
export interface IUserOptions {
  uid: number;
}

export namespace vitessr {
  export type ContainerType = 'koa' | 'express' | 'egg';

  export type ScriptType = 'js' | 'ts' | 'cjs' | 'mjs';

  export interface EntryFileOptions {
    /**
     * 客户端入口文件 默认 entry_client 不包含扩展名
     */
    client?: string;
    /**
     * 服务端入口文件 默认 entry_server 不包含扩展名
     */
    server?: string;
  }

  export interface DevOptions {
    /**
     * 源码目录，相对于 {root}
     */
    src?: string;
    /**
     * 脚本类型
     */
    script?: ScriptType;
    /**
     * vite 调试服务选项
     */
    devServerOpts?: DevServerOptions;
  }

  export interface ProdOptions {
    /**
     * 脚本类型
     */
    script: ScriptType;
    /**
     * 编译输出目录，默认 dist， 路径：{root}/{dist}
     */
    dist?: string;
    /**
     * 客户端构建输出目录 默认 client，路径：{root}/{dist}/{client}
     */
    client?: string;
    /**
     * 服务端构建输出目录 默认 server，路径：{root}/{dist}/{server}
     */
    server?: string;
  }

  export type DevServerOptions = InlineConfig;

  export type StaticOptions = serve.Options;

  export type RenderData = {
    /**
     *  App.vue 渲染结果
     */
    appHtml: string;
    /**
     * 预加载资源链接
     */
    preloadLinks?: string;
    /**
     * 页面标题
     */
    title?: string;
    /**
     * csrf 漏洞防御信令
     */
    csrfToken?: string;
    /**
     * csrfToken 名称
     */
    csrfTokenName?: string;
    /**
     * 其他增量的渲染数据
     */
    [key: string]: any;
  };

  export type CustomTemplateRenderHandle = (
    ctx: Context,
    template: string,
    data: RenderData
  ) => string;

  export interface Options {
    /**
     * 页面标题
     */
    pageTitle?: string;
    /**
     * csrfToken 名称 默认 __ct__
     */
    csrfTokenName?: string;
    /**
     * vite ssr 项目访问路径 默认 '/'
     */
    prefix?: string;
    /**
     * vite ssr 项目目录 默认 'web'，相对于 appDir
     */
    root?: string;
    /**
     * 入口文件
     */
    entry?: EntryFileOptions;
    /**
     * SSR 资源清单文件路径 默认 ssr-manifest  {root}/{dist}/{client}/.vite/{manifest}.json
     */
    manifest?: string;
    /**
     * html 模板文件名，默认 index.html
     */
    indexHtml?: string;
    /**
     * 容器类型
     */
    container?: ContainerType;
    /**
     *  SSR 生产配置
     */
    prod?: ProdOptions;
    /**
     *  SSR 开发配置
     */
    dev?: DevOptions;
    /**
     * 静态文件中间件选项
     */
    staticOpts?: StaticOptions;
    /**
     * 自定义模板渲染句柄
     */
    templateRender?: CustomTemplateRenderHandle;
  }
}

declare module '@midwayjs/core/dist/interface.js' {
  interface MidwayConfig {
    vitessr?: vitessr.Options;
  }
}
