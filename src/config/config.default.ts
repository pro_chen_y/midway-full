import { MidwayConfig } from '@midwayjs/core';

export default {
  // use for cookie sign key, should change to your own and keep security
  keys: '1700026301074_5296',
  koa: {
    port: 7001,
  },
  vitessr: {
    pageTitle: 'MidwayFull',
    csrfTokenName: '__ct__',
    prefix: '/',
    root: 'web',
    manifest: 'ssr-manifest',
    indexHtml: 'index.html',
    container: 'koa',
    entry: {
      client: 'entry-client',
      server: 'entry-server',
    },
    prod: {
      script: 'js',
      dist: 'dist',
      client: 'client',
      server: 'server',
    },
    dev: {
      script: 'ts',
      src: 'src',
      devServerOpts: {
        root: 'web',
        server: {
          middlewareMode: true,
          watch: {
            // During tests we edit the files too fast and sometimes chokidar
            // misses change events, so enforce polling for consistency
            usePolling: true,
            interval: 100,
          },
        },
        appType: 'custom',
      },
    },
    staticOpts: { index: false },
  },
} as MidwayConfig;
