import { Provide } from '@midwayjs/core';
import {
  App,
  Config,
  Inject,
  MidwayWebRouterService,
  FileUtils,
  Singleton,
} from '@midwayjs/core';
import { Application, Context } from '@midwayjs/koa';
import koaStatic from 'koa-static';
import mount from 'koa-mount';
import { resolve as _resolve } from 'path';
import { readFileSync } from 'fs';
import artTemplate from 'art-template';

import koaConnect from '../utils/koaConnect.js';
import {
  DevRenderError,
  DirectoryNotFoundError,
  ProdRenderError,
  TemplateRenderError,
} from '../error.js';
import { vitessr } from '../interface.js';

export const defaultTemplateRender = (
  ctx: Context,
  template: string,
  data: vitessr.RenderData
) => {
  try {
    ctx.status = 200;
    ctx.headers['content-type'] = 'text/html';
    return artTemplate.render(template, data);
  } catch (e) {
    throw new TemplateRenderError(e.stack);
  }
};

@Provide()
@Singleton()
export class ServerSideRenderService {
  @App()
  private app: Application;

  @Inject()
  private webRouterService: MidwayWebRouterService;

  @Config('vitessr')
  private config: vitessr.Options;

  private called = false;

  private vite = null;

  async startup() {
    if (this.called) return;

    const { app, config } = this;
    const isProd = this.app.getEnv() === 'production';
    const resolvePath = (p: string) => _resolve(app.getAppDir(), p);
    const templateRender = config.templateRender ?? defaultTemplateRender;

    const {
      prefix,
      root,
      manifest,
      entry,
      indexHtml,
      staticOpts,
    }: vitessr.Options = config;

    let template = null;
    let render = null;
    let ssrManifest = null;
    let fnSSRMiddleware: any;

    if (isProd) {
      const { dist, client, server, script } = config.prod;
      const clientOutDir = `${root}@${dist}/${client}`;
      const staticPath = resolvePath(clientOutDir);
      const isExists = await FileUtils.exists(staticPath);
      if (!isExists) {
        throw new DirectoryNotFoundError(staticPath);
      }
      fnSSRMiddleware = mount(prefix, koaStatic(staticPath, staticOpts));

      template = readFileSync(`${staticPath}/${indexHtml}`, 'utf-8');
      ssrManifest = JSON.parse(
        readFileSync(`${staticPath}/.vite/${manifest}.json`, 'utf-8')
      );

      const entryFile = resolvePath(
        `${root}@${dist}/${server}/${entry.server}.${script}`
      );
      render = (await import(`file://${entryFile}`)).render;
    } else {
      const { createServer } = await import('vite');
      const { devServerOpts } = config.dev;
      devServerOpts.root = config.root;
      this.vite = await createServer(devServerOpts);
      fnSSRMiddleware = koaConnect(this.vite.middlewares);
    }

    fnSSRMiddleware._name = 'SSRMiddleware';
    app.useMiddleware(fnSSRMiddleware);

    const { src, script } = config.dev;
    const all = async (ctx: Context) => {
      const url = ctx.originalUrl;
      try {
        if (!isProd && this.vite) {
          template = readFileSync(resolvePath(`${root}/${indexHtml}`), 'utf-8');
          template = await this.vite.transformIndexHtml(url, template);
          render = (
            await this.vite.ssrLoadModule(`/${src}/${entry.server}.${script}`)
          ).render;
        }

        const [appHtml, preloadLinks] = await render(
          url,
          ssrManifest,
          this.vite?.moduleGraph
        );

        const data: vitessr.RenderData = {
          appHtml,
          preloadLinks,
          title: config.pageTitle,
          csrfTokenName: config.csrfTokenName,
          csrfToken: ctx.csrfToken ?? '',
        };

        ctx.body = templateRender(ctx, template, data);
      } catch (e) {
        ctx.status = 500;
        if (this.vite) {
          this.vite.ssrFixStacktrace(e);
          throw new DevRenderError(e.stack);
        }
        throw new ProdRenderError(e.stack);
      }
    };

    this.webRouterService.addRouter(all, {
      url: '/*',
      requestMethod: 'GET',
    });

    this.called = true;
  }

  stop() {
    if (this.vite) {
      return this.vite.close();
    }
  }
}
