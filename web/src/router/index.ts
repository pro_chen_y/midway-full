import { createRouter as _createRouter, createMemoryHistory, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/AboutView.vue')
  }
]

export function createRouter() {
  const createHistory = import.meta.env.SSR ? createMemoryHistory : createWebHistory;
  return _createRouter({
    history: createHistory(import.meta.env.BASE_URL),
    routes
  })
}
