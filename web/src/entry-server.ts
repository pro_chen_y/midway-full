import { basename } from 'node:path'
import { renderToString } from 'vue/server-renderer'
import { ModuleGraph, ModuleNode } from 'vite'

import { createApp } from './main'

export async function render(url: string, manifest: any, moduleGraph: ModuleGraph) {
  const { app, router } = createApp()

  await router.push(url)
  await router.isReady()

  const ctx: any = {}
  const html = await renderToString(app, ctx)

  let preloadLinks = ''
  if (manifest) {
    preloadLinks = renderPreloadLinks(ctx.modules, manifest)
  } else if (moduleGraph) {
    preloadLinks = await renderPreloadLinksForDevMode(moduleGraph)
  }

  return [html, preloadLinks]
}

function isScriptFile(filePath: string) {
  return /\.(c|m)?(j|t)sx?$/.test(filePath)
}

function isTemplate(file: string) {
  return file && file.endsWith('.vue')
}

function isAssets(url: string) {
  const [baseurl] = url.split('?')
  if (!isScriptFile(baseurl)) {
    if (isTemplate(baseurl)) {
      return /\?vue&type=style/.test(url)
    } else {
      return true
    }
  }
  return false
}

async function renderPreloadLinksForDevMode(moduleGraph: ModuleGraph) {
  const { urlToModuleMap } = moduleGraph ?? {}
  if (urlToModuleMap) {
    const assetsUrls = new Set<string>()
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    urlToModuleMap.forEach((value: ModuleNode, key: string, map: Map<string, ModuleNode>) => {
      const { file } = value
      if (isAssets(key)) {
        const link = renderPreloadLink(key, file && isTemplate(file) ? '?direct' : '')
        if (link) {
          assetsUrls.add(link)
        }
      }
    })
    return Array.from(assetsUrls).join('\n')
  }
  return ''
}

function renderPreloadLinks(modules: Array<any>, manifest: any) {
  let links = ''
  const seen = new Set()
  modules.forEach((id) => {
    const files: Array<string> = manifest[id]
    if (files) {
      files.forEach((file) => {
        if (!seen.has(file)) {
          seen.add(file)
          const filename = basename(file)
          if (manifest[filename]) {
            for (const depFile of manifest[filename]) {
              links += renderPreloadLink(depFile)
              seen.add(depFile)
            }
          }
          links += renderPreloadLink(file)
        }
      })
    }
  })
  return links
}

function renderPreloadLink(file: string, suffix: string = '') {
  if (file.endsWith('.js')) {
    return `<link rel="modulepreload" crossorigin href="${file}">`
  } else if (file.endsWith('.css')) {
    return `<link rel="stylesheet" href="${file + suffix}">`
  } else if (file.endsWith('.woff')) {
    return ` <link rel="preload" href="${file}" as="font" type="font/woff" crossorigin>`
  } else if (file.endsWith('.woff2')) {
    return ` <link rel="preload" href="${file}" as="font" type="font/woff2" crossorigin>`
  } else if (file.endsWith('.gif')) {
    return ` <link rel="preload" href="${file}" as="image" type="image/gif">`
  } else if (file.endsWith('.jpg') || file.endsWith('.jpeg')) {
    return ` <link rel="preload" href="${file}" as="image" type="image/jpeg">`
  } else if (file.endsWith('.png')) {
    return ` <link rel="preload" href="${file}" as="image" type="image/png">`
  } else if (file.endsWith('.icon')) {
    return ` <link rel="preload" href="${file}" as="image" type="image/x-icon">`
  } else if (file.endsWith('.svg')) {
    return ` <link rel="preload" href="${file}" as="image" type="image/svg+xml">`
  } else {
    return ''
  }
}
