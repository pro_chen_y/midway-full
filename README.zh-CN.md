# my_midway_project

## 快速入门

<!-- 在此次添加使用文档 -->

如需进一步了解，参见一下连接：
* [Midway 文档][midway]
* [Midway ESModule使用指南][midway_esm]
* [Vite 文档][vite]
* [Vite 服务端渲染][vitessr]
* [Vue3 文档][vue3]

### 本地开发

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### 部署

```bash
$ npm start
```

### 内置指令

- 使用 `npm run lint` 来做代码风格检查。
- 使用 `npm test` 来执行单元测试。


[midway]: https://midwayjs.org/docs/intro
[midway_esm]: https://midwayjs.org/docs/esm
[vite]: https://cn.vitejs.dev/guide/
[vitessr]: https://cn.vitejs.dev/guide/ssr.html 
[vue3]: https://cn.vuejs.org/guide/introduction.html
