# Midway Full Stack

## QuickStart

<!-- add docs here for user -->
Here are some useful links:
* [Midway docs][midway]
* [Midway ESModule usage guide][midway_esm]
* [Vite docs][vite]
* [Vite Server-Side Rendering][vite ssr]
* [Vue3 docs][vue3]

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm run build
$ npm start
```

### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.

[midway]: https://midwayjs.org/en/docs/intro
[midway_esm]: https://midwayjs.org/en/docs/esm
[vite]: https://vitejs.dev/guide/
[vite ssr]: https://vitejs.dev/guide/ssr.html 
[vue3]: https://vuejs.org/guide/introduction.html
